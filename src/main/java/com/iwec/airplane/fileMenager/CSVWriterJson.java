package com.iwec.airplane.fileMenager;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class CSVWriterJson {
	public void write(String fileName, List<String> content) {
		try (PrintWriter out = new PrintWriter(fileName)) {
			out.write(content.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}

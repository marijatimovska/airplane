package com.iwec.airplane.fileMenager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public abstract class FileMenagerImpl<T> implements FileMenager<T> {

	public List<T> read(String file) {
		List<T> results = new LinkedList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(new File(file)))) {
			String line;

			while ((line = br.readLine()) != null) {
				T t = createInstance(line);
				if (t != null) {
					results.add(t);
				}
			}

		} catch (IOException e) {
			System.err.println("Error while reading file" + file);
		}
		return results;
	}

	public abstract T createInstance(String line);
	

}

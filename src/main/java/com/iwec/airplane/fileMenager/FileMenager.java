package com.iwec.airplane.fileMenager;

import java.util.List;

public interface FileMenager<T> {
	public List<T> read(String file);

}

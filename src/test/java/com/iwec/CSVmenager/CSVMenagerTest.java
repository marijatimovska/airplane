package com.iwec.CSVmenager;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.iwec.airplane.model.Airplane;
import com.iwec.csvMenager.CSVMenager;

@ExtendWith(MockitoExtension.class)
class CSVMenagerTest {

	@Spy
	private List<Airplane> spyList = new ArrayList<>();

	@TempDir
	File temp;
	
	@InjectMocks
	private CSVMenager csvMenager;

	@BeforeEach
	public void setUp() {
		csvMenager = new CSVMenager();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testWhenListHasElements() {

		spyList.add(new Airplane(1, "Proba", "Proba", 250, 27000.0));
		Mockito.verify(spyList).add(new Airplane(1, "Proba", "Proba", 250, 27000.0));

		temp = new File("test.csv");
		String s = "1,Proba,Proba,250,27000.0";
		csvMenager.csvMenager(spyList, temp);
		
		assertEquals(s, temp.canRead());
		
		
	    
	 
	}

}
